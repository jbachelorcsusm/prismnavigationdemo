# Intro to Prism Navigation #

Welcome! This repo is designed as a learning exercise about hierarchical navigation using the Prism framework. At the LearningExerciseStartPoint tag, this project includes a number of "TODO:" comments, numbered in a suggested order of completion. Clone this repository to complete the exercise. See below for more information about what tags are available, and what this exercise is designed to teach.

### What does this exercise teach? ###

* Navigation without hard-coded strings.
* Implementation of the INavigationAware interface.
* Page registration.
* How to get an instance of the Prism NavigationService in a ViewModel.
* Use of the Prism NavigationService to navigate to a new page.
* Using Prism's NavigationParameters to pass data from viewModel to viewModel.
* Wiring up Commands between View and ViewModel.

### Git Tags On This Repository
* Tags can be found on Bitbucket by clicking on 'Downloads', then click the 'Tags' option.
* Alternatively, you can click [here.](https://bitbucket.org/jbachelorcsusm/prismnavigationdemo/downloads/?tab=tags)
* [LearningExerciseStartPoint](https://bitbucket.org/jbachelorcsusm/prismnavigationdemo/commits/tag/LearningExerciseStartPoint):  This is the place to start this learning exercise. Download this to start the exercise over again.
* [ClassCode2018.02.19](https://bitbucket.org/jbachelorcsusm/prismnavigationdemo/commits/tag/ClassCode2018.02.19):  Half-way point.
* [LearningExerciseComplete](https://bitbucket.org/jbachelorcsusm/prismnavigationdemo/commits/tag/LearningExerciseComplete):  This is the code at the completion of the exercise.
