﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Prism.Navigation;
using Prism.Commands;
using PrismNavigationDemo.Views;

namespace PrismNavigationDemo.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware 
    {
        #region Services and fields for internal use by this class

        INavigationService _navigationService;

        #endregion Services and fields for internal use by this class



        #region Properties

        public DelegateCommand NavToNeatPageCommand { get; set; }

        private string _title;
        /// <summary>
        /// Gets or sets the title of the page this ViewModel supports. This is an example of a property
        /// that implements INotifyPropertyChanged, using the ViewModelBase class provided by Prism.
        /// </summary>
        /// <value>The title of the page</value>
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        #endregion Properties and Fields



        #region Constructor

        public MainPageViewModel(INavigationService navigationService)  
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MainPageViewModel)}:  ctor");

            Title = "Launch Page";
            _navigationService = navigationService;
            NavToNeatPageCommand = new DelegateCommand(OnNavToNeatPage);
        }

        #endregion Constructor



        #region INavigationAware implementation

		public void OnNavigatedFrom(NavigationParameters parameters)
		{
			Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
		}
		
		public void OnNavigatedTo(NavigationParameters parameters)
		{
			Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
		}
		
		public void OnNavigatingTo(NavigationParameters parameters)
		{
			Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
		}

        #endregion INavigationAware implementation



        #region Methods

		private void OnNavToNeatPage()
		{
			Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavToNeatPage)}");

            var navParams = new NavigationParameters();
            navParams.Add("importantData", DateTime.Now);

            _navigationService.NavigateAsync(nameof(NeatPage), navParams);
		}

        #endregion Methods
    }
}
