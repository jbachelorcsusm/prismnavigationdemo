﻿using System;
using System.Diagnostics;
using Prism.Mvvm;
using Prism.Navigation;

namespace PrismNavigationDemo.ViewModels
{
    public class NeatPageViewModel : BindableBase, INavigationAware
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public NeatPageViewModel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NeatPageViewModel)}:  ctor");
            Title = "Neat Sauce";
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
            if (parameters.ContainsKey("importantData"))
            {
                Debug.WriteLine($"The time is: {parameters["importantData"]}");
            }
        }
    }
}
